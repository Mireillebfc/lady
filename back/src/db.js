import sqlite from 'sqlite';
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {
  
  const db = await sqlite.open('./db.sqlite');

  const createEvent = async (props) => {
    if(!props || !props.name || !props.date || !props.location || !props.information){
      throw new Error(`you must provide a name, a date, a location and information`)
    }
    const { name, date, location, information } = props
    try{
      const result = await db.run(SQL`INSERT INTO events (name,date, location, information) VALUES (${name}, ${date}, ${location}, ${information})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  const createCD = async (props) => {
    if(!props || !props.title || !props.price){
      throw new Error(`you must provide a title and a price`)
    }
    const { title, price } = props
    try{
      const result = await db.run(SQL`INSERT INTO CDs (title,price) VALUES (${title}, ${price})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  const deleteEvent = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM events WHERE rowid = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`event "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the event "${id}": `+e.message)
    }
  }
  const deleteCD = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM CDs WHERE rowid = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`CD "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the CD "${id}": `+e.message)
    }
  }
  const updateEvent = async (id, props) => {
    if (!props || !(props.name || props.date || props.location || props.information)) {
      throw new Error(`you must provide a name , a date, a location or new information`);
    }
    const { name, date, location, information } = props;
    try {
      let statement = "";
      if (name && date && location && information) {
        statement = SQL`UPDATE events SET date=${date}, name=${name} , location=${location}, information=${information} WHERE rowid = ${id}`;
      } else if (name) {
        statement = SQL`UPDATE events SET name=${name} WHERE rowid = ${id}`;
      } else if (date) {
        statement = SQL`UPDATE events SET date=${date} WHERE rowid = ${id}`;
      }else if (location) {
        statement = SQL`UPDATE events SET location=${location} WHERE rowid = ${id}`;
      } else if (information) {
        statement = SQL`UPDATE events SET information=${information} WHERE rowid = ${id}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the contact ${id}: ` + e.message);
    }
  }
  const updateCD = async (id, props) => {
    if (!props || !(props.title || props.price)) {
      throw new Error(`you must provide a title , a price`);
    }
    const { title, price } = props;
    try {
      let statement = "";
      if ( title && price) {
        statement = SQL`UPDATE CDs SET title=${title}, price=${price} WHERE rowid = ${id}`;
      } else if (title) {
        statement = SQL`UPDATE events SET title=${title} WHERE rowid = ${id}`;
      } else if (price) {
        statement = SQL`UPDATE events SET title=${price} WHERE rowid = ${id}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the CD ${id}: ` + e.message);
    }
  }
  const getEventsList = async () => {
   const rows = await db.all("SELECT rowid AS id, name, date, location, information FROM events")
    return rows;
  }

  const getCDsList = async () => {
    const myRows = await db.all("SELECT rowid AS id,title, price FROM CDs")
    return myRows;
  }

  const getEvent = async (id) => {
    try{
      const eventsList = await db.all(SQL`SELECT rowid AS id, name, date, location, information FROM events WHERE rowid = ${id}`);
      const event = eventsList[0]
      if(!event){
        throw new Error(`event ${id} not found`)
      }
    return event
    }catch(e){
      throw new Error(`couldn't get the event ${id}:`+e.message)
    }
  }

  const getCD = async (id) => {
    try{
      const CDsList = await db.all(SQL`SELECT rowid AS id, title, price FROM CDs WHERE rowid = ${id}`);
    const CD = CDsList[0]
    if(!CD){
      throw new Error(`CD ${id} not found`)
    }
    return CD
  }catch(e){
    throw new Error (`couldn't get the CD ${id}:`+e.message)
  }
  }

  const controller = {
    getEventsList,
    getCDsList,
    getEvent,
    getCD,
    createEvent,
    deleteEvent,
    updateEvent,
    createCD,
    deleteCD,
    updateCD
  }

  return controller
}

export default initializeDatabase;



