import app from './app'
import initializeDatabase from './db'
import { authenticateUser, logout, isLoggedIn } from './auth'


const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get("/events/new", async (req, res, next) => {
    try {
      const { name, date, location, information } = req.query;
      const result = await controller.createEvent({ name, date, location, information });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  app.get("/CDs/new", async (req, res, next) => {
    try {
      const { title, price } = req.query;
      const result = await controller.createCD({ title, price });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  
  app.get('/events/get/:id', async (req, res, next) => {
    const { id } = req.params
    const event = await controller.getEvent(id)
    res.json({success:true, result:event})
  })
  app.get('/CDs/get/:id', async (req, res, next) => {
    const { id } = req.params
    const CD = await controller.getCD(id)
    res.json({success:true, result:CD})
  })

  app.get("/events/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteEvent(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  app.get("/CDs/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteCD(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  app.get("/events/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { name, date, location, information } = req.query;
      const result = await controller.updateEvent(id, { name, date, location, information });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  app.get("/CDs/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { title, price } = req.query;
      const result = await controller.updateCD(id, { title, price });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  app.get('/events/list', async (req, res) => {
    const events_list = await controller.getEventsList()
    res.json({success:true, result:events_list})
  })
  app.get('/CDs/list', async (req, res) => {
    const CDs_list = await controller.getCDsList()
    res.json({success:true, result:CDs_list})
  })

  //app.get('/login', authenticateUser)
  
  //app.get('/logout', logout)
  
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.name
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })

  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success:false, message })
  })
  app.listen(8080, () => console.log('server listening on port 8080'))
}
start();

