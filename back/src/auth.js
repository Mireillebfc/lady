
import jwt from 'express-jwt'
import jwksRsa from 'jwks-rsa'

const users = [
    { nick:'ladybug', name:'lady', password:'bug' }]
  
  
  
  const tokens = {}
  
  export const authenticateUser = (req, res, next) => {
    if(!req.query.username || !req.query.password){
      return res.status(401).json({
        success:false,
        message:'username and password are both necessary'
      })
    }
    const { username, password } = req.query
    
    const userIndex = users.findIndex( u => u.password === password && u.name === username)
    if(userIndex < 0 ){
      return res.status(401).json({
        success: false,
        message:'wrong username or password'
      })
    }
    
    const user = users[userIndex]
    const nick = user.nick
    const token = Math.random()+"" 
    tokens[token] = userIndex
    res.json({
      success:true,
      result: {
        nick,
        token
      }
    })
  }
  
  export const logout = (req, res, next) => {
    const token = req.query.token
    if(!token){
      
      return res.json({ success: true })
    }
    if(typeof tokens[token] === 'undefined'){
      
      return res.json({ success: true })
    }
    
    delete tokens[token]
    return res.json({ success:true })
  }
  

  /*export const isLoggedIn = (req, res, next) => {
    const token = req.query.token
    if(!token || (typeof tokens[token] === 'undefined')){
      return res.status(403).json({ success: false, message: 'forbidden' })
    }
    const userIndex = tokens[token]
    const user = users[userIndex]
    req.is_logged_in = true
    req.user = user 
    next()
  }*/
  const AUTH0_DOMAIN = 'mireille.auth0.com'
  const AUTH0_CLIENT_ID = 'GeFRut9IPA4qCawHEZiQTpNu2M31r0sn'

  export const isLoggedIn = jwt({
    secret: jwksRsa.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://${mireille.auth0.com}/.well-known/jwks.json`
    }),
    audience: GeFRut9IPA4qCawHEZiQTpNu2M31r0sn,
    issuer: `https://${mireille.auth0.com}/`,
    algorithms: ['RS256']
  });
    