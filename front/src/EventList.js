import React from "react";
import { Transition } from "react-spring"; 
import { Link } from "react-router-dom";

const EventList = ({ events_list }) => (
  <Transition
    items={events_list}
    keys={event => event.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { event => style => (
      <div style={style}>
        <Link to={"/event/"+event.id}>{event.name}</Link>
      </div>
    )}
  </Transition>
);

export default EventList