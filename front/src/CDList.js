import React from "react";
import { Transition } from "react-spring";
import { Link } from "react-router-dom";

const CDList = ({ CDs_list }) => (
  <div>
  <Transition
    items={CDs_list}
    keys={CD => CD.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { CD => style => (
      <div style={style}>
        <Link to={"/CD/"+CD.id}>{CD.title}</Link>
      </div>
    )}
  </Transition>
  </div>
);

export default CDList