import auth0 from "auth0-js";

const AUTH0_DOMAIN = 'mireille.auth0.com'
const AUTH0_CLIENT_ID = 'GeFRut9IPA4qCawHEZiQTpNu2M31r0sn'

let idToken = null;
let profile = null;
let expiresAt = null;

const auth0Client = new auth0.WebAuth({
  domain: AUTH0_DOMAIN,
  audience: `https://${AUTH0_DOMAIN}/userinfo`,
  clientID: AUTH0_CLIENT_ID,
  redirectUri: "http://localhost:3000/callback",
  responseType: "id_token",
  scope: "openid profile"
});

export const handleAuthentication = async () => {
    return new Promise((resolve, reject) => {
      auth0Client.parseHash((err, authResult) => {
        if (err){ return reject(err);}
        if(!authResult || !authResult.idToken){ return reject(new Error('user was not registered'))}
        idToken = authResult.idToken;
        profile = authResult.idTokenPayload;
        // set the time that the id token will expire at
        expiresAt = (authResult.expiresIn || 1000) * 1000 + new Date().getTime();
        console.log(authResult)
        resolve(profile);
      });
    });
  }

  export const signOut = ()  => {
    idToken = null;
    profile = null;
    expiresAt = null;
  }
  export const getProfile = () => profile;

  export const getIdToken = () => idToken;

  export const isAuthenticated = () => new Date().getTime() < expiresAt;

  export const signIn = () => auth0Client.authorize();