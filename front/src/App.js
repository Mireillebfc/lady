import React from 'react';
import CD from './CD';
import Event from './event'
//import { Transition } from 'react-spring'
import { Switch, Route, Link ,withRouter} from "react-router-dom";
import EventList from "./EventList";
import CDList from "./CDList";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { pause, makeRequestUrl } from "./utils.js";
import './App.css';
import * as auth0Client from './auth';

const makeUrl = (path, params) => makeRequestUrl(`http://localhost:8080/${path}`,params)

class App extends React.Component {
  state = {
    events_list: [], 
    CDs_list: [], 
    error_message: "", 
    title: "",
    price: "",
    name: "", 
    date: "", 
    location: "", 
    information: "",
   /* token: null,
    nick: null,*/
    isLoading:false
  }

  getEvent = async id => {
    const previous_event = this.state.events_list.find(
      event => event.id === id
    );
    if (previous_event) {
      return;
    }
    try {
      const url = makeUrl(`events/get/${id}`, {token: this.state.token})
      const response = await fetch(url);

      
      const answer = await response.json();
      if (answer.success) {

        const event = answer.result;
        const events_list = [...this.state.events_list, event];
        this.setState({ events_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getCD = async id => {

    const previous_CD = this.state.CDs_list.find(
      cd => cd.id === id
    );
    if (previous_CD) {
      return;
    }
    try {
      const url = makeUrl(`cds/get/${id}`, { token: this.state.token });
      const response = await fetch(url);
            const answer = await response.json();
      if (answer.success) {

        const cd = answer.result;
        const CDs_list = [...this.state.CDs_list, cd];
        this.setState({ CDs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteEvent = async id => {
    try {
      const url = makeUrl(`events/delete/${id}`, { token: this.state.token });
      const response = await fetch(url);
    
      const answer = await response.json();
      if (answer.success) {
        const events_list = this.state.events_list.filter(
          event => event.id !== id
        );
        this.setState({ events_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  deleteCD = async id => {
    try {
      const url = makeUrl(`CDs/delete/${id}`, { token: this.state.token });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {

        const CDs_list = this.state.CDs_list.filter(
          cd => cd.id !== id
        );
        this.setState({ CDs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateEvent = async (id, props) => {
    try {
      if (!props || !(props.name || !props.date || !props.location || !props.information)) {
        throw new Error(`you need at least name or date or location or information properties to update an event`
        );
      }
      const url = makeUrl(`events/update/${id}`, {name:props.name,date:props.date,location:props.location,information:props.information, token:this.state.token});
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        const events_list = this.state.events_list.map(event => {

          if (event.id === id) {
            const new_event = {
              id: event.id,
              name: props.name || event.name,
              date: props.date || event.date,
              location: props.location || event.location,
              information: props.information || event.information
            };
            return new_event;
          }

          else {
            return event;
          }
        });
        this.setState({ events_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  updateCD = async (id, props) => {
    try {
      if (!props || !(props.title || !props.price)) {
        throw new Error(`you need at least title or price properties to update a CD`
        );
      }
      const url = makeUrl(`CDs/update/${id}`, {title:props.title,price:props.price, token:this.state.token});
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {

        const CDs_list = this.state.CDs_list.map(cd=> {

          if (cd.id === id) {
            const new_CD = {
              id: cd.id,
              title: props.title||cd.title,
              price:props.price||cd.price
            };
            return new_CD;
          }

          else {
            return CD;
          }
        });
        this.setState({ CDs_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  createEvent = async props => {
    try {
      if (!props || !(props.name&& props.date && props.location && props.information)) {
        throw new Error(
          `you need name, date, location and information properties to create an event`
        );
      }
      const { name, date, location, information } = props;
      const url = makeUrl(`events/new`, {name,date,location,information,token:this.state.token});
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {

        const id = answer.result;
        const event = { name, date, location, information, id };
        const events_list = [...this.state.events_list, event];
        this.setState({ events_list });
        toast("Event was created!");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  }
    ;

    createCD = async props => {
      try {
        if (!props || !(props.title && props.price)) {
          throw new Error(
            `you need both title and price properties to create a CD`
          );
        }
        const { title, price } = props;
        const url = makeUrl(`CDs/new`, {title,price,token:this.state.token});
        const response = await fetch(url);
        const answer = await response.json();
        if (answer.success) {

          const id = answer.result;
          const cd = { title, price, id };
          const CDs_list = [...this.state.CDs_list, cd];
          this.setState({ CDs_list });
        } else {
          this.setState({ error_message: answer.message });
        }
      } catch (err) {
        this.setState({ error_message: err.message });
      }
    };
    getEventsList = async () => {
      this.setState({ isLoading:true })
      try {
        const url = makeUrl(`events/list`, {token: this.state.token });
      const response = await fetch(url);
        await pause()
        const answer = await response.json();
        if (answer.success) {
          const events_list = answer.result;
          this.setState({ events_list, isLoading:false  });
          toast("events loaded");
        } else {
          this.setState({ error_message: answer.message , isLoading:false  });
          toast.error(answer.message);
        }
      } catch (err) {
        this.setState({ error_message: err.message, isLoading:false  });
       
      }
    };
    getCDsList = async () => {
      this.setState({ isLoading:true })
      try {
        const url = makeUrl(`CDs/list`, { token: this.state.token });
      const response = await fetch(url);
        await pause()
        const answer = await response.json();
          if (answer.success) {
          const CDs_list = answer.result;
          this.setState({ CDs_list, isLoading:false  });
          toast("CDs loaded");
        } else {
          this.setState({ error_message: answer.message, isLoading:false  });
          toast.error(answer.message); 
        }
      } catch (err) {
        this.setState({ error_message: err.message, isLoading:false  });
      }
    }
    /*login = async (username, password) => {
      try {
        const url = makeUrl(`login`, {
          username,
          password,
          token: this.state.token
        });
        const response = await fetch(url);
        const answer = await response.json();
        if (answer.success) {
          const { token, nick } = answer.result;
          this.setState({ token, nick });
          toast(`successful login`);
        } else {
          this.setState({ error_message: answer.message });
          toast.error(answer.message);
        }
      } catch (err) {
        this.setState({ error_message: err.message });
        toast.error(err.message);
      }
    };
    logout = async token => {
      try {
        const url = makeUrl(`logout`, { token: this.state.token });
        const response = await fetch(url);
        const answer = await response.json();
        if (answer.success) {
          this.setState({ token: null, nick: null });
          toast(`successful logout`);
        } else {
          this.setState({ error_message: answer.message });
          toast.error(answer.message);
        }
      } catch (err) {
        this.setState({ error_message: err.message });
        toast.error(err.message);
      }
    };*/

    getPersonalPageData = async () => {
      try {
        const url = makeUrl(`mypage`, { token: this.state.token });
        const response = await fetch(url);
        const answer = await response.json();
        if (answer.success) {
          const message = answer.result;
          
          toast(`received from the server: '${message}'`);
        } else {
          this.setState({ error_message: answer.message });
          toast.error(`error message received from the server: ${answer.message}`);
        }
      } catch (err) {
        this.setState({ error_message: err.message });
        toast.error(err.message);
      }
    };
    componentDidMount(){
      this.getEventsList();
      this.getCDsList(); 
    }

    onSubmit1 = evt => {
      evt.preventDefault();

      const { name, date, location, information } = this.state;

      this.createEvent({ name, date, location, information });

      this.setState({ name: "", date: "", location: "", information: "" })
    };


    onSubmit2 = evt => {
      evt.preventDefault();

      const { title, price } = this.state;

      this.createCD({ title, price });

      this.setState({ title: "", price: "" });
    };

    /*onLoginSubmit = evt => {
      evt.preventDefault();
      const username = evt.target.username.value;
      const password = evt.target.password.value;
      if (!username) {
        toast.error("username can't be empty");
        return;
      }
      if (!password) {
        toast.error("password can't be empty");
        return;
      }
      this.login(username, password);
    };*/
    renderUser() {
      const isLoggedIn = auth0Client.isAuthenticated()
    if (isLoggedIn) {

        return this.renderUserLoggedIn();
      } else {
        return this.renderUserLoggedOut();
      }
    }
    renderUserLoggedOut() {
      return (
        <button onClick={auth0Client.signIn}>Sign In</button>
      );
    }
    renderUserLoggedIn() {
      const { nick } = auth0Client.getProfile().name
      return (
        <div>
          Hello, {nick}! <button onClick={()=>{auth0Client.signOut();this.setState({})}}>logout</button>
        </div>
      );
    }
    renderEventListPage = () => {
    const { events_list } = this.state;
    return <EventList events_list={events_list} />};
     
    renderCDListPage = () => {
    const { CDs_list } = this.state;
    return <CDList CDs_list={CDs_list} />};
      
    renderCDPage = ({ match }) => {
          const id = match.params.id;
          // eslint-disable-next-line
          const cd = this.state.CDs_list.find(cd => cd.id == id);
            if (!cd) {
            return <div>{id} not found</div>;
          }
          return (
            <CD
              id={cd.id}
              title={cd.title}
              price={cd.price}
              updateCD={this.updateCD}
              deleteCD={this.deleteCD}
            />
          );
        }
        renderEventPage = ({ match }) => {
          const id = match.params.id;
          // eslint-disable-next-line
          const event= this.state.events_list.find(event => event.id == id);
          if (!event) {
            return <div>{id} not found</div>;
          }
          return (
            <Event
              id={event.id}
              name={event.name}
              date={event.date}
              location={event.location}
              information={event.information}
              updateEvent={this.updateEvent}
              deleteEvent={this.deleteEvent}
            />
          );
        }
        renderProfilePage = () => {
          return (
            <div>
              <p>profile page</p>
              {this.renderUser()}
            </div>
          );
        }
        
        renderCreateEventForm = () => {
          return (<div>
            <h1>Events</h1>
            <form onSubmit={this.onSubmit1}>
          <input
            type="text"
            placeholder="name"
            onChange={evt => this.setState({ name: evt.target.value })}
            value={this.state.name}
          />
          <input
            type="text"
            placeholder="date"
            onChange={evt => this.setState({ date: evt.target.value })}
            value={this.state.date}
          />
          <input
            type="text"
            placeholder="location"
            onChange={evt => this.setState({ location: evt.target.value })}
            value={this.state.location}
          />
          <input
            type="text"
            placeholder="information"
            onChange={evt => this.setState({ information: evt.target.value })}
            value={this.state.information}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
        </div>)
                }
        renderCreateCDForm = () => {
          return (<div>
          <h1>CDs</h1>
          <form onSubmit={this.onSubmit2}>
          <input
            type="text"
            placeholder="title"
            onChange={evt => this.setState({ title: evt.target.value })}
            value={this.state.title}
          />
          <input
            type="text"
            placeholder="price"
            onChange={evt => this.setState({ price: evt.target.value })}
            value={this.state.price}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
        </div>)
        }  
        
      renderContent() {
          if (this.state.isLoading) {
            return <p>loading...</p>}

          return (
            <Switch>
              {/*<Route path="/" exact render={this.renderHomePage} />*/}
              <Route path="/EventList"  render={this.renderEventListPage} />
              <Route path="/CDList"  render={this.renderCDListPage} />
              <Route path="/event/:id" render={this.renderEventPage} />
              <Route path="/CD/:id" render={this.renderCDPage} />
              <Route path="/" exact render={this.renderProfilePage} />
              <Route path="/createEvent" render={this.renderCreateEventForm} />
              <Route path="/createCD" render={this.renderCreateCDForm} />
              <Route path="/callback" render={this.handleAuthentication} />
              <Route render={()=><div>not found!</div>}/>
            </Switch>
          );
          }
          login = async (history) => {
            try{
              await auth0Client.handleAuthentication();
              console.log(auth0Client.isAuthenticated(),auth0Client.getProfile())
              const name = auth0Client.getProfile().name
              toast(`${name} is logged in`)
              history.push('/profile')
            }catch(err){
              toast.error(err.message);
            }
          }
          handleAuthentication = ({history}) => {
            this.login(history)
            return <p>wait...</p>
          }

        render() {  
      return (     
              <div className="App">       
          <img src={require("./Ladybug.jpg")} alt= "i am the ladybug"/>
              {/*<Link to="/">Home</Link> | */}
              <Link to="/">profile</Link> |
             {/* <Link to="/event">Event</Link> |
              <Link to="/CD">CD</Link> |*/}
              <Link to="/EventList">Events</Link> |
              <Link to="/CDList">CDs</Link> |
              <Link to="/createEvent">Create Event</Link> |
              <Link to="/createCD">Create CD</Link>
              {this.renderContent()}
          <ToastContainer />
        </div>
        
      );
    }
  }

  export default withRouter(App);
