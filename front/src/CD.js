import React from 'react'

export default class CD extends React.Component {
  state = {








    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };
  renderViewMode() {
    const { id, title,price, deleteCD} = this.props;
  
    return (
      <div>
        <span>
          {id} - {title}-{price}
        </span>
        <button onClick={this.toggleEditMode} className="success">
          edit
        </button>
        <button onClick={() => deleteCD(id)} className="warning">
          x
        </button>
      </div>
    );
  }
  renderEditMode() {
    const { title, price } = this.props;
    return (
      <form
        className="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="name"
          name="CD_title_input"
          defaultValue={title}
        />
        <input
          type="text"
          placeholder="price"
          name="CD_price_input"
          defaultValue={price}
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
    
    evt.preventDefault();
   
    const form = evt.target;
    
    const CD_title_input = form.CD_title_input;
    const CD_price_input = form.CD_price_input;
   
    const title = CD_title_input.value;
    const price = CD_price_input.value;
    
    const { id, updateCD } = this.props;
   
    updateCD(id, { title, price });
    
    this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      
      return this.renderViewMode();
    }
  }
}