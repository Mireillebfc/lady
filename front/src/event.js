import React from 'react'

export default class Event extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };
  renderViewMode() {
    const { id, name,date, location, information, deleteEvent } = this.props;
    return (
      <div>
        <span>
          {id} - {name}-{date}-{location}-{information}
        </span>
        <button onClick={this.toggleEditMode} className="success">
          edit
        </button>
        <button onClick={() => deleteEvent(id)} className="warning">
          x
        </button>
      </div>
    );
  }
  renderEditMode() {
    const { name, date, location, information } = this.props;
    return (
      <form
        className="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="name"
          name="Event_name_input"
          defaultValue={name}
        />
        <input
          type="text"
          placeholder="date"
          name="Event_date_input"
          defaultValue={date}
        />
        <input
          type="text"
          placeholder="location"
          name="Event_location_input"
          defaultValue={location}
          />
          <input
          type="text"
          placeholder="information"
          name="Event_information_input"
          defaultValue={information}
                />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
  
    evt.preventDefault();
    
    const form = evt.target;
    
    /* const Event_name_input = form.Event_name_input;
    const Event_date_input = form.Event_date_input;
    const Event_information_input = form.Event_information_input;
    const Event_location_input = form.Event_location_input; */
   
    const name = form.Event_name_input.value;
    const date = form.Event_date_input.value;
    const location = form.Event_location_input.value;
    const information = form.Event_information_input.value;
    
    const { id, updateEvent } = this.props;
    updateEvent(id, { name, date, location, information });
    
    this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}