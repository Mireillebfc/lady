const test = async () => {

  const db = await sqlite.open('./db.sqlite');
  await db.run(`CREATE TABLE events (name TEXT NOT NULL UNIQUE, date TEXT NOT NULL, location TEXT NOT NULL, information TEXT NOT NULL);`);

const stmt = await db.prepare(SQL`INSERT INTO events VALUES (?,?,?,?)`);
let i = 0;
while(i<5){
  await stmt.run(`event${i}`,`date${i}`,`location${i}`, `information${i}`);
  i++
}

await stmt.finalize();


const rows = await db.all("SELECT rowid AS id, name, date, location, information FROM events")
rows.forEach( row => console.log(`[id:${row.id}] - ${row.name} - ${row.date} - ${row.location} - ${row.information}`) )
}
export default {test};